<?php

use App\Http\Controllers\EditController;
use App\Http\Controllers\EditControllers;
use App\Http\Controllers\HomeControllers;
use App\Http\Controllers\TambahdataControllers;
use App\Http\Controllers\UpdateControllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// delete and show data
Route::get('/',[HomeControllers::class, 'dashboard']);
Route::delete('/{id}',[HomeControllers::class, 'hapus']);
// end

// edit data
Route::get('/edit/{id}',[EditController::class, 'editdata' ]);
Route::patch('/{id}',[EditController::class, 'prosesedit']);
// end

// add data
Route::get('/tambah-data',[TambahdataControllers::class, 'Add']);
Route::post('/',[TambahdataControllers::class, 'processadd']);
// end
@extends('layout/main')

@section('title', 'Dashboard')

@section('breadcrumbs')
<div class="breadcrumbs" style="margin-top: -4px; background-color: rgba(7, 7, 7, 0.603)">
    <div class="col-sm-4">
        <div class="page-header float-left" style="background-color: rgba(26, 26, 26, 0.555)">
            <div class="page-title text-light">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right" style="background-color: rgba(26, 26, 26, 0.555)">
            <div class="page-title">
                <ol class="breadcrumb text-right" style="background-color: rgba(26, 26, 26, 0.555)">
                    <li class="active"><i class="fa fa-dashboard text-light"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <strong>Daftar Mahasiswa</strong>
                </div>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-bordered ">
                    <thead class="bg-dark text-light">
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIM</th>
                            <th>Kelas</th>
                            <th>Prodi</th>
                            <th>Fakultas</th>
                            <th class="text-center">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($mahasiswa as $tampil)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $tampil->nama_mahasiswa }}</td>
                                <td>{{ $tampil->nim_mahasiswa }}</td>
                                <td>{{ $tampil->kelas_mahasiswa }}</td>
                                <td>{{ $tampil->prodi_mahasiswa }}</td>
                                <td>{{ $tampil->fakultas_mahasiswa }}</td>
                                <td class="text-center">
                                    <a href="{{ url('edit',$tampil->id) }}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    &ensp;
                                    <form action="{{ url('/'.$tampil->id) }}" method="POST" onsubmit="return confirm('HAPUS DATA INI?')" class="d-inline">
                                        @method('delete')
                                        @csrf
                                        <button class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>    
@endsection
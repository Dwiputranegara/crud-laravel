@extends('layout/main')

@section('title', 'Tambah Data')

@section('breadcrumbs')
<div class="breadcrumbs" style="margin-top: -4px; background-color: rgba(7, 7, 7, 0.603)">
    <div class="col-sm-4">
        <div class="page-header float-left" style="background-color: rgba(26, 26, 26, 0.555)">
            <div class="page-title text-light">
                <h1>Tambah Data</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right" style="background-color: rgba(26, 26, 26, 0.555)">
            <div class="page-title">
                <ol class="breadcrumb text-right" style="background-color: rgba(26, 26, 26, 0.555)">
                    <li class="active"><i class="fa fa-user-plus text-light"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <strong>Tambah Data Mahasiswa</strong>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ url('/') }}" method="POST">
                        @csrf
                            <div class="form-group">
                                <label for="nama">Nama Mahasiswa</label>
                                <input type="text" name="nama" class="form-control" placeholder="masukan nama lengkap anda!">
                            </div>
                         
                            <div class="form-group">
                                <label for="nama">NIM Mahasiswa</label>
                                <input type="text" name="nim" class="form-control" placeholder="masukan nim anda!">
                            </div>

                            <div class="form-group">
                                <label for="nama">Kelas Mahasiswa</label>
                                <input type="text" name="kelas" class="form-control" placeholder="masukan kelas anda!">
                            </div>

                            <div class="form-group">
                                <label for="nama">Prodi Mahasiswa</label>
                                <input type="text" name="prodi" class="form-control" placeholder="masukan prodi anda!">
                            </div>
                         
                            <div class="form-group">
                                <label for="nama">Fakultas Mahasiswa</label>
                                <input type="text" name="fakultas" class="form-control" placeholder="masukan fakultas anda!">
                            </div>
                         
                            <button type="submit" class="btn btn-success" style="border-radius: 5px">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
    </div>
</div>
</div>    
@endsection
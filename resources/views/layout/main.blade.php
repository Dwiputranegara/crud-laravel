<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')-Laravel CRUD Querry Builder</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- icons --}}
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    {{-- CSS --}}
    <link rel="stylesheet" href="{{ asset('style/assets/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('style/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style/assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('styleassets/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('style/assets/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style/assets/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ asset('style/assets/scss/style.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>
<body>
    {{-- script js --}}
    <script src="{{ asset('style/assets/js/vendor/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('style/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('style/assets/js/plugins.js') }}"></script>
    <script src="{{ asset('style/assets/js/main.js') }}"></script>
    {{-- end script js --}}

    {{-- Side Bar --}}
    <aside id="left-panel" class="left-panel" style="background-color: black">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: black">
            <div class="navbar-header" style="background-color: black">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="/">
                <img src="img/ffg.png" alt="" style="width: 60px; height: 60px; margin-left:-2rem">CRUD Mahasiswa</a>
                <a class="navbar-brand hidden" href="">
                    <img src="img/ffg.png" alt="" style="width: 30px; height: 30px;">
                </a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse" style="background-color: black">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="/"> <i class="menu-icon fa fa-dashboard"></i>Dashboard</a>
                    </li>
                    <li>
                        <a href="/tambah-data"> <i class="menu-icon fa fa-user-plus"></i>Tambah Data</a>
                    </li>
                </ul>
            </div>
        </nav>
    </aside>
    {{-- end side bar --}}

        {{-- header --}}
    <div id="right-panel" class="right-panel bg-dark">
        <header id="header" class="header" style="background-color: black">
            <div class="header-menu">
                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search text-light"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2 text-light" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>
                        <div class="dropdown for-notification">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell text-light"></i>
                            <span class="count bg-danger">1</span>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="notification">
                            <p class="red">You Have New Notifications</p>
                            <a class="dropdown-item media bg-flat-color-3" href="#">
                                <i class="fa fa-check text-dark"></i>
                                <p class="text-dark">CRUD Laravel!.</p>
                            </a>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="{{ asset('style/images/Addmin.png') }}">
                        </a>
                        <div class="user-menu dropdown-menu bg-dark">
                            <a class="nav-link text-light" href="#"><i class="fa fa-cog ms-2"></i> &ensp; Settings</a>
                            <a class="nav-link text-light" href="#"><i class="fa fa-power-off ms-2"></i> &ensp; Logout</a>
                        </div>
                    </div>
                    <div class="language-select dropdown" id="language-select">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                            <i class="flag-icon flag-icon-id"></i>
                        </a>
                        <div class="dropdown-menu bg-dark" aria-labelledby="language" >
                            <div class="dropdown-item">
                                <span class="flag-icon flag-icon-id"></span>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-es"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-us"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-jp"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        {{-- end Header --}}

@yield('breadcrumbs')
@yield('content')

</body>
</html>
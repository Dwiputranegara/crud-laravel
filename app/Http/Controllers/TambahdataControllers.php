<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TambahdataControllers extends Controller
{
    public function Add()
    {
     return view('crud/tambah');
    }

    public function processadd(Request $request)
    {
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa'        => $request->nama,
            'nim_mahasiswa'         => $request->nim,
            'kelas_mahasiswa'       => $request->kelas,
            'prodi_mahasiswa'       => $request->prodi,
            'fakultas_mahasiswa'    => $request->fakultas
        ]);
        return redirect('/')->with('status', 'Data Mahasiswa Berhasil Ditambahkan!');
    }
}

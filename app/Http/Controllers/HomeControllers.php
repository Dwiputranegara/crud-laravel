<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeControllers extends Controller
{
    public function dashboard()
    {
     $mahasiswa = DB::table('mahasiswa')->get();
     return view('crud/home', ['mahasiswa'=>$mahasiswa]);
    }

    public function hapus($id)
    {
        DB::table('mahasiswa')->where('id', $id)->delete();
        return redirect('/')->with('status', 'Data Mahasiswa Telah Dihapus!');
    }
}

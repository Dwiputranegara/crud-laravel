<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EditController extends Controller
{
    public function editdata($id)
    {
        $edutdat = DB::table('mahasiswa')->where('id', $id)->first();
        // @dd($edutdat);
        return view('crud/edit', compact('edutdat'));
    }

    public function prosesedit(Request $request, $id)
    {
        DB::table('mahasiswa')->where('id', $id)
        ->update([
            'nama_mahasiswa'        => $request->nama,
            'nim_mahasiswa'         => $request->nim,
            'kelas_mahasiswa'       => $request->kelas,
            'prodi_mahasiswa'       => $request->prodi,
            'fakultas_mahasiswa'    => $request->fakultas
        ]);
        return redirect('/')->with('status', 'Data Mahasiswa Berhasil Diubah!');
    }
}
